//!wrt $BSPEC:{"frn":"Apple Swift for Windows 96","dsc":"Apple Swift has came to Windows 96 (real)","cpr":"(C) Themirrazz 2022 - GNU GPL 3.0","aut":"Themirrazz"}
const {Theme}=w96.ui;
var compile=function compileSwiftFull(swift, ob) {
    try {
        var libraries=ob.modules||{};
        var inject=ob.inject||""
    } catch (error) {
        var libraries={};
        var inject=""
    }
    var newLine=true;
    var isString=false;
    var inInString=false;
    var comment=false;
    var iis_par=0;
    var varName=false;
    var func=false;
    var slc=false;
    var While=false;
    var WhileWait=false;
    var WhileBK=0;
    var If=false;
    var IfWait=false;
    var IfBK=0;
    var getColAndRow=function(text,cw) {
        var row=1;
        var col=1;
        for(var i=0;i<text.length;i++) {
            if(text[i]==="\n") {
                row++;
                col=1
            } else {
                col++
            }
            if(cw==i){
                return {row,col}
            }
        }
    }
    var x=swift.trim().split("\n");
    x.forEach((e,i)=>{x[i]=e.trim()})
    var _swift=x.join("\n");
    var result="";
    for(var i=0;i<swift.length;i++) {
        if(slc) {
            if(swift[i]==="\n") {
                slc=false
            }
            result+=swift[i]
        } else if(comment) {
            if((swift[i]==="/")&&(swift[i-1]==="*")) {
                comment=false
            }
            result+=swift[i]
        } else if(inInString) {
            if(swift[i]===")") {
                if(iis_par<1) {
                    inInString=false;
                    result+=")}";
                } else {
                    iis_par--;
                }
            } else if(swift[i]==="(") {
                iis_par++;
                result+="("
            } else {
                result+=swift[i]
            }
        } else if(isString) {
            if(swift[i]==='"') {
                if(swift[i-1]==="\\") {
                    result+='"'
                } else {
                    result+='`'
                    isString=false;
                }
            } else if((swift[i]==="(")&&(swift[i-1]==="\\")) {
                inInString=true;
                result+="${String("
                iis_par=0;
            } else if(swift[i]==="\\") {
                null;
            } else {
                result+=swift[i]
            }
        } else {
            if(
                (swift[i-1]==" "||newLine)&&swift.slice(i).indexOf("func ")===0
            ) {
                if(swift.slice(i).split(" ")[2]==='async') {
                    result+='async '
                }
                result+='function '
                func=true;
                i+=4
            } else if(
                ((swift[i-1]==" "||newLine)&&swift.slice(i).indexOf("async ")===0)
            ) {
                result+=''
                i+=5
            } else if(
                (swift[i-1]==" "||newLine)&&swift.slice(i).indexOf("let ")===0
            ) {
                result+='const '
                i+=2
            } else if(
                (swift[i-1]==" "||newLine)&&swift.slice(i).indexOf("while ")===0
            ) {
                result+='while ('
                WhileWait=true
                i++;
                i++
                i++
                i++
            } else if(
                (swift[i-1]==" "||newLine)&&swift.slice(i).indexOf("if ")===0
            ) {
                result+='if ('
                i++
                IfWait=true
            } else if(func&&(swift[i]=="-"&&swift[i+1]==">")) {
                result+=''
                i+=(swift.slice(i+1).indexOf("{"))-1
            } else if(swift[i]==='"') {
                isString=true;
                result+='`';
            } else {
                if((swift[i]==="{")&&func) {
                    func=true;
                }
                if(While&&swift[i]==="}") {
                    WhileBK--
                    if(WhileBK<0){While=false}
                }
                if(If&&swift[i]==="}") {
                    IfBK--
                    if(IfBK<0){While=false}
                }
                if((swift[i]==="{")&&WhileWait) {
                    result+=")"
                    WhileWait=false;
                    While=true
                    WhileBK=0;
                }
                if((swift[i]==="{")&&IfWait) {
                    result+=")"
                    IfWait=false;
                    If=true
                    IfBK=0;
                }
                result+=swift[i];
            }
        }
        if(swift[i]==="\n") {
            newLine=true;
            func=false
        } else {
            newLine=false;
        }
    }
    var o=[];
    var canI=true
    result.split('\n').forEach(line=>{
        if(line.startsWith("@import ")) {
            if(!canI) {
                throw new SyntaxError("@import must be declared at beginning of the file");
            } else {
                try {
                    libraries[line.slice(8)].forEach(e=>{
                        o.push(`const ${e.name} = ${e.source}`)
                    })
                } catch (FE){
                    throw new ReferenceError(`the library '${line.slice(8)}' does not exist`)
                }
            }
        } else {
            if(line.trim().length>0) {
                if(canI) {
                    canI=false;
                    o.push(inject)
                }
            }
            o.push(line)
        }
    });
    result=o.join("\n");
    o=result.split("\n");
    if(comment) {
        throw new SyntaxError(`expected closing comment, line ${i+1} column 1`);
    }
    return result
};

var terminal=current.boxedEnv.term;
var tm=terminal?true:false;
function show_error(t,m,o) {
    if(tm) {
        terminal.println(
            terminal.color.red(`${t}: ${m}`)
        )
    } else {
        w96.ui.MsgBoxSimple.error(t,m,"OK")
    }
}


class AppleSwift extends WApplication {
    constructor(){ super() }
    async main(argv) {
        var path=argv[1];
        if(!path) {
            show_error("ENONT", "Please specify a path to an Apple Swift script.", 'wat');
            return this.terminate();
        }
        if(!await w96.FS.exists(path)) {
            show_error("ENONT", "The path specified does not exist.");
            return this.terminate()
        }
        if(!await w96.FS.isFile(path)) {
            show_error("ENONT", 'The path specified is a directory.')
            return this.terminate()
        }
        var term=tm?terminal:await w96.util.requestTerminal();
        run(compile(await w96.FS.readstr(path),{
            modules:{
                UI: [
                    {
                        name: 'UIWindow',
                        source:'function(p){return new StandardWindow(p)};'
                    }
                ]
            },
            inject:`(function(){var setTimeout,setInterval,Function,w96,parent,window,location,Object,Error,TypeError,
SyntaxError,ReferenceError,EvalError,navigator,document,w96bld,u96,terminal,
Math;`
        }));
    }
}

function run(js) {
    eval(`var UI;
    (function(){
    ${js};
    })();
    })();`);
}

return await WApplication.execAsync(new AppleSwift(),this.boxedEnv.args,this);
